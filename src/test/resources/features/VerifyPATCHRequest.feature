Feature: Check update functionality for existing user data using PATCH method
#Test Case 4  PATCH REQUEST
  Scenario: Run the PATCH Request and update the existing data to verify existing user records is update or not.

    Given the data that need to be update
    When run the PATCH request with data
    Then verify the PATCH response 200 status code
    
