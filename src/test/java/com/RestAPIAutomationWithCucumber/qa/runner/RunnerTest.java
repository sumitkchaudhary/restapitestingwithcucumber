/**
 * Project Copyright:    Ingenico India Pvt Ltd.
 * @author 			:	 ksumit
 *	DATE       		:	 18-Jun-2023
 *  FILE NAME  		: 	 RunnerTest.java
 *  PROJECT NAME 	:	 mediationapp
 * 	Time			:    12:45:13 pm
 */
package com.RestAPIAutomationWithCucumber.qa.runner;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.AfterClass;
import org.junit.runner.RunWith;


@RunWith(Cucumber.class)
@CucumberOptions(
		publish = false,
		features={"src/test/resources/features"},
		glue="com.RestAPIAutomationWithCucumber.qa.stepdefs",
		plugin= {"pretty",
				"json:target/cucumber.json"
		}
		)

public class RunnerTest {

	@AfterClass
    public static void setup() {
    }

}
