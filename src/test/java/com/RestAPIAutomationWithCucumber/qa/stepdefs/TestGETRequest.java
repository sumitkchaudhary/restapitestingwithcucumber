package com.RestAPIAutomationWithCucumber.qa.stepdefs;

import com.RestAPIAutomationWithCucumber.qa.commoncontrollers.HTTPMethodController;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class TestGETRequest extends HTTPMethodController {

    @Given("to me url till the users")
    public void to_me_url_till_the_users(){
        handleGiven();
    }
    @When("run the GET request")
    public void run_the_get_request(){
        res=handleWhenFor_GET(pr.getProperty("endurl"));
    }
    @Then("verify the {int}")
    public void verify_the(int statusCode){
        handleThen().statusCode(statusCode);
    }

}
