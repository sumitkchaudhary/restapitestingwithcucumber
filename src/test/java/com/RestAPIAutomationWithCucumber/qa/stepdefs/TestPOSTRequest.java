package com.RestAPIAutomationWithCucumber.qa.stepdefs;

import com.RestAPIAutomationWithCucumber.qa.commoncontrollers.All_IN_ONE_Controller;
import com.RestAPIAutomationWithCucumber.qa.commoncontrollers.HTTPMethodController;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.cucumber.java.en.Then;
import org.json.JSONObject;

public class TestPOSTRequest extends HTTPMethodController {
    JSONObject requestPayload;
    @Given("to me post url")
    public void to_me_post_url() {
        requestPayload= All_IN_ONE_Controller.readJsonFile("userPayload.json");

        handleGiven();
    }

    @When("run the POST request with data")
    public void run_the_post_request_with_data() {
        res=handleWhenFor_Post(requestPayload.toString(),pr.getProperty("endurl"));
    }

    @Then("verify the post response {int} status code")
    public void verify_the_post_response_status_code(Integer statusCode) {
        handleThen().statusCode(statusCode);

    }



}
