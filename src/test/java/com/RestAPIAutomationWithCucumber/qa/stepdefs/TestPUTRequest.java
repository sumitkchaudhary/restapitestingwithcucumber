package com.RestAPIAutomationWithCucumber.qa.stepdefs;

import com.RestAPIAutomationWithCucumber.qa.commoncontrollers.All_IN_ONE_Controller;
import com.RestAPIAutomationWithCucumber.qa.commoncontrollers.HTTPMethodController;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.json.JSONObject;

public class TestPUTRequest extends HTTPMethodController {
    JSONObject requestPayload;
    @Given("to me data that need to be update")
    public void to_me_data_that_need_to_be_update() {
        requestPayload= All_IN_ONE_Controller.readJsonFile("userPayload.json");

        handleGiven();
    }

    @When("run the PUT request with data")
    public void run_the_put_request_with_data() {
        res=handleWhenFor_PUT(requestPayload.toString(), pr.getProperty("endurl")+"/2");
    }

    @Then("verify the PUT response {int} status code")
    public void verify_the_put_response_status_code(Integer statusCode) {
        handleThen().statusCode(statusCode);

    }



}
