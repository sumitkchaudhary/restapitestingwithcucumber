package com.RestAPIAutomationWithCucumber.qa.stepdefs;

import com.RestAPIAutomationWithCucumber.qa.commoncontrollers.All_IN_ONE_Controller;
import com.RestAPIAutomationWithCucumber.qa.commoncontrollers.HTTPMethodController;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.json.JSONObject;

public class TestPATCHRequest extends HTTPMethodController {
    JSONObject requestPayload;
    @Given("the data that need to be update")
    public void the_data_that_need_to_be_update() {
        requestPayload= All_IN_ONE_Controller.readJsonFile("userPayload.json");

        handleGiven();
    }

    @When("run the PATCH request with data")
    public void run_the_patch_request_with_data() {
        res=handleWhenFor_PATCH(requestPayload.toString(), pr.getProperty("endurl")+"/2");
    }

    @Then("verify the PATCH response {int} status code")
    public void verify_the_patch_response_status_code(Integer statusCode) {
        handleThen().statusCode(statusCode);

    }



}
