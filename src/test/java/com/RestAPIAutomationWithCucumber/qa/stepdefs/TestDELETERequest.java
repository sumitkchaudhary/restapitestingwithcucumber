package com.RestAPIAutomationWithCucumber.qa.stepdefs;

import com.RestAPIAutomationWithCucumber.qa.commoncontrollers.HTTPMethodController;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class TestDELETERequest extends HTTPMethodController {
    @Given ("to me delete url till the user id")
    public void to_me_delete_url_till_the_user_id(){
        handleGiven();
    }
    @When( "run the delete request with user id")
    public void run_the_delete_request_with_user_id(){
        res=handleWhenFor_DELETE(pr.getProperty("endurl")+"/2");
    }
    @Then ("verify the delete response {int} status code")
    public void verify_the_delete_response_status_code(int statusCode){
        handleThen().statusCode(statusCode);
    }

}
