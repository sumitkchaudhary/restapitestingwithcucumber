/**
 * Project Copyright:    Ingenico India Pvt Ltd.
 * @author 			:	 ksumit
 *	DATE       		:	 18-Jun-2023
 *  FILE NAME  		: 	 HTTPMethodController.java
 *  PROJECT NAME 	:	 mediationapp
 * 	Time			:    4:24:32 pm
 */
package com.RestAPIAutomationWithCucumber.qa.commoncontrollers;

import static io.restassured.RestAssured.given;
import java.util.Properties;
import com.RestAPIAutomationWithCucumber.qa.utilities.MasterController;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;

public class HTTPMethodController {
	public static RequestSpecification reqSpec;
	public static Response res;
	public static ValidatableResponse valRes;
	public static Properties pr= MasterController.loadProperties();

	public static RequestSpecification handleGiven() {
		return reqSpec= given().
				baseUri(pr.getProperty("baseurl"))
				.accept(ContentType.JSON);
	}

	/**
	 * @return
	 */
	public static ValidatableResponse handleThen() {

		return valRes=res.then();
	}
	public static Response handleWhenFor_GET(String endURL) {
		return res=reqSpec
				.when()
				.get(endURL);
	}

	public static Response handleWhenFor_Post(String requestBody, String endURL) {
		return  reqSpec.body(requestBody) // pass the expected data
				.when()
				.post(endURL);
	}

	public static Response handleWhenFor_PUT(String requestBody, String endURL) {
		return reqSpec.body(requestBody) // pass the expected data
				.when()
				.put(endURL);
	}
	public static Response handleWhenFor_PATCH(String requestBody, String endURL) {
		return reqSpec.body(requestBody) // pass the expected data
				.when()
				.patch(endURL);
	}

	public static Response handleWhenFor_DELETE(String endURL) {
		return reqSpec// pass the expected data
				.when()
				.delete(endURL);
	}

}
