/**
 * Project Copyright:    Ingenico India Pvt Ltd.
 * @author 			:	 ksumit
 *	DATE       		:	 03-Sep-2023
 *  FILE NAME  		: 	 All_IN_ONE_Controller.java
 *  PROJECT NAME 	:	 mediationapp
 * 	Time			:    12:03:40 pm
 */
package com.RestAPIAutomationWithCucumber.qa.commoncontrollers;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import com.RestAPIAutomationWithCucumber.qa.utilities.MasterController;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.json.XML;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class All_IN_ONE_Controller extends HTTPMethodController {

			public static JSONObject readJsonFile (String fileName) {
		
		 JSONObject convertToJSonObject = null;
		try {
			convertToJSonObject = new JSONObject(new JSONTokener(new FileReader(MasterController.getAbsolutPath(fileName))));
		} catch (Exception e) {
			e.printStackTrace();
			e.getStackTrace();
			e.getMessage();
		}
		return convertToJSonObject;
	}
	

	public static String convertJSON_TO_XML(String fileName) throws IOException
	{
		
		FileReader readFile = new FileReader(MasterController.getAbsolutPath(fileName));
		
		JSONTokener parseFile = new JSONTokener(readFile);
		
		JSONObject storeObj = new JSONObject(parseFile);
		
		String xmlConvert = XML.toString(storeObj);
		
		return xmlConvert;
		 
	}
	
	public static JSONObject covertXML_TO_JSON (String fileName) throws IOException 
	{
		FileReader readFile = new FileReader(MasterController.getAbsolutPath(fileName));
		
		@SuppressWarnings("resource")
		BufferedReader getFileData = new BufferedReader(readFile);
		
		String store ; 
		String increaseData=null;
		
		while ((store=getFileData.readLine())!=null)
		{
			increaseData+=store;
		}
		JSONObject convertedJSOn = XML.toJSONObject(increaseData);
		
		return convertedJSOn;
	}
	public static String responseDataParse(Response responseData, String jsonPath){
		//return the value as per given path/ key
		JsonPath jsnPath=responseData.jsonPath();
		return jsnPath.getString(jsonPath);
	}
	
	public static String fetchDatafromJSON(String jsonBody, String srcArrKey, String srhKey, String srhValue, String gtExtData) {
		JSONObject jsonData = new JSONObject(jsonBody);				// Get the jSon body and store in jSon object 
		JSONArray getArray = jsonData.getJSONArray(srcArrKey);		//Fetch the array value on behalf search key and store in array variable 
		JSONObject getObject=null;									//Taking here one more jSON Object variable for further use
		for (int i=0 ; i<getArray.length(); i++) {					//run the loop end of the array length
			getObject = getArray.getJSONObject(i);					//get the all object inner on array 
			for(int j=0; j<getObject.length(); j++) { 				//run the loop end of the object length {
				
				if(getObject.getString(srhKey).contains(srhValue)) {
					break;
				}
			}
		}
		//get the expected data on behalf search key and value and store in jSon object variable which i taking and  before run the loop and convert in string  data type
		return  getObject.get(gtExtData).toString();	
	}
}
